(() => {
    const onNewsChange = () => {
        let firstChild, lastChild;
        const prevArrow = document.querySelector("#news-prev");
        const nextArrow = document.querySelector("#news-next");
        const news = document.querySelector(".news-animate ul");

        document.addEventListener("click", () => {
            if(event.target === prevArrow) {
                lastChild = news.lastElementChild;
                news.insertAdjacentElement("afterbegin", lastChild);
            } else if (event.target === nextArrow) {
                firstChild = news.firstElementChild;
                news.insertAdjacentElement("beforeend", firstChild);
            }
        })
    };

    const preventOverflow = () => {
        let element = document.documentElement;
        if(element.scrollHeight > element.clientHeight) {
            // overflow detected
            element.style.overflow = 'scrollbar';
        } else {
            // no overflow detected
            element.style.overflow = 'hidden';
        }
    };

    // Change menu block with selection
    const menuChange = () => {
        const elements = document.querySelectorAll('.menu-item');
        const fruitTeaDiv = document.querySelector('#fruitTeaMenu');
        const herbalTeaDiv = document.querySelector('#herbalTeaMenu');
        const smoothiesDiv = document.querySelector('#smoothiesMenu');
        const snowIceDiv = document.querySelector('#snowIceMenu');

        function toggleSwitch() {
            const currentEl = document.querySelectorAll('.active.menu-item')[0];
            const currentMenu = document.querySelectorAll('.ui.segment.menu-display')[0];

            // Switch block if the selection is not current display
            if (currentEl !== this) {
                currentEl.classList.remove('active');
                this.classList.add('active');
                currentMenu.classList.remove('menu-display');
                currentMenu.classList.add('menu-hidden');

                // Select by menu title and change display state by class
                if (this.textContent === 'Fruit Tea') {
                    fruitTeaDiv.classList.remove('menu-hidden');
                    fruitTeaDiv.classList.add('menu-display');
                } else if (this.textContent === 'Herbal Tea') {
                    herbalTeaDiv.classList.remove('menu-hidden');
                    herbalTeaDiv.classList.add('menu-display');
                } else if (this.textContent === 'Smoothies / Slush') {
                    smoothiesDiv.classList.remove('menu-hidden');
                    smoothiesDiv.classList.add('menu-display');
                } else if (this.textContent === 'Snow Ice / Shaved Ice') {
                    snowIceDiv.classList.remove('menu-hidden');
                    snowIceDiv.classList.add('menu-display');
                }
            }
        }

        elements.forEach( el => el.addEventListener('click', toggleSwitch));
    };

    preventOverflow();
    onNewsChange();
    menuChange();
})();
