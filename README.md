# ISNOW

## Introduction

iSnow is a local boba tea shop in Tempe Arizona. I designed and built this website for presenting all the products in the shop.

## Screenshot

![screenshot](https://github.com/trnet4334/ISNOW/blob/master/images/isnow.png) 

## Demo

Check it out [live](https://isnow.shop/)!

## Technologies
   
This page was built by using the following technologies: 

* Html
* CSS (Sass)
* JQuery
* JavaScript ES6

## Task list

- [x] Page layout\
- [x] Styling Header, main content section\
- [ ] Make whole page responsive\
- [ ] Animation on menu block\